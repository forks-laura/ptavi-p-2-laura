#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from calcoo import Calc

class CalcChild(Calc):

    def mul(self, num1, num2):
        """Función de multiplicacion"""
        return num1 * num2

    def div(self, num1, num2):
        """"Funcion de dividir"""
        try:
            return num1 / num2
        except ZeroDivisionError:
            print("Division by zero is not allowed")
        raise ValueError("Division by zero is not allowed")

if __name__ == "__main__":
    try:
        num1 = float(sys.argv[1])
        num2 = float(sys.argv[3])
        objeto1 = Calc()
        objeto2 = CalcChild()
    except ValueError:
        sys.exit("Error: No son parametros numericos")

    if sys.argv[2] == "+":
        resultado = objeto1.add(num1, num2)
        print(resultado)
    elif sys.argv[2] == "-":
        resultado = objeto1.sub(num1, num2)
        print(resultado)
    elif sys.argv[2] == "x":
        resultado = objeto2.mul(num1, num2)
        print(resultado)
    elif sys.argv[2] == "/":
        resultado = objeto2.div(num1, num2)
        print(resultado)
    else:
        sys.exit('Error: tiene que ser "+", "-", "x" o "/"')

