#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from calccount import Calc


def process_csv(fichero):
    # Abrimos y leemos el fichero
    with open(fichero, "r") as fich:
        lineas = fich.readlines()  # Nos devuelve una lista con cada línea del fichero

    for contenido in lineas:
        elementos = contenido.split(",")  # El split nos devuelve una lista diviendo la cadena contenido(de cada linea)
        error = False
        operacion = elementos[0]
        try:
            valores = list(map(float, elementos[1:]))
        except ValueError:
            print("Bad format")
        num1 = valores[0]
        for j in valores[1:]:
            num2 = float(j)
            objeto = Calc()

            if operacion == "+":
                try:
                    resultado = objeto.add(float(num1), float(num2))
                    num1 = resultado
                except ValueError:
                    print("Bad format")
                    error = True
                    break

            elif operacion == "-":
                try:
                    resultado = objeto.sub(num1, num2)
                    num1 = resultado
                except ValueError:
                    print("Bad format")
                    error = True
                    break

            elif operacion == "*":
                try:
                    resultado = objeto.mul(num1, num2)
                    num1 = resultado
                except ValueError:
                    print("Bad format")
                    error = True
                    break

            elif operacion == "/":
                try:
                    resultado = objeto.div(num1, num2)
                    num1 = resultado
                except ValueError:
                    print("Division by zero is not allowed")
                    error = True
                    break
            else:
                print("Bad format")
        if not error:
            print(num1)



if __name__ == "__main__":
    process_csv(sys.argv[1])














