#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

class Calc():

    def __init__(self):
        """Inicializador"""
        self.count = 0

    def add(self, num1, num2):
        """Funcion suma"""
        self.count = self.count + 1
        return num1 + num2

    def sub(self, num1, num2):
        """Funcion resta"""
        self.count = self.count + 1
        return num1 - num2

    def mul(self, num1, num2):
        """Función multiplicacion"""
        self.count = self.count + 1
        return num1 * num2

    def div(self, num1, num2):
        """"Funcion division"""
        self.count = self.count + 1
        try:
            return num1 / num2
        except ZeroDivisionError:
            raise ValueError("Division by zero is not allowed")
