#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

class Calc():

    def add(self, num1, num2):
        """Funcion suma"""
        return num1 + num2

    def sub(self, num1, num2):
        """Funcion resta"""
        return num1 - num2

if __name__ == "__main__":
    try:
        num1 = float(sys.argv[1])
        num2 = float(sys.argv[3])
        objeto = Calc()
    except ValueError:
        sys.exit("Error: No son parametros numericos")

    if sys.argv[2] == "+":
        resultado = objeto.add(num1, num2)
        print(resultado)
    elif sys.argv[2] == "-":
        resultado = objeto.sub(num1, num2)
        print(resultado)
    else:
        sys.exit("Error: tiene que ser + o -")